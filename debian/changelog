smstools (3.1.21-4) unstable; urgency=medium

  * take care of gcc10 errors (Closes: #957818)
  * debian/control: bump standard to 4.5.0 (no changes)
  * debian/control: use dh12
  * debian/patches/spelling.patch: add more stuff noted by lintian

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 05 Aug 2020 21:39:26 +0200

smstools (3.1.21-3) unstable; urgency=medium

  * move package to salsa
  * debian/control: use dh11
  * debian/control: bump standard to 4.1.5 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 16 Jul 2018 19:39:26 +0200

smstools (3.1.21-2) unstable; urgency=medium

  * move to unstable
  * debian/control: bump standard to 4.1.1 (no changes)
  * debian/control: move maintainer to mobcom
  * debian/control: add VCS URLs

 -- Thorsten Alteholz <debian@alteholz.de>  Fri, 13 Oct 2017 19:39:26 +0200

smstools (3.1.21-1) experimental; urgency=medium

  * new upstream version
  * Version >= 3.1.16 now respects TMPDIR and TEMPDIR variables.
    (Closes: #635717)
  * Version 3.1.17 can now use inotifywait, if user wants and needs
    to use it (smsd.conf: notifier = yes/no).
    (Closes: #703687)
  * CPU usage spikes fixed in 3.1.16 (Closes: #688451)
  * using "modern" rules file and docu should be available on all archs
    (Closes: #593934)
  * Zombie-issue is fixed in the version >= 3.1.16.
    (Closes: #569346)
  * Message delay is fixed in the version >= 3.1.16.
    (Closes: #755898)

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 10 May 2017 19:39:26 +0200

smstools (3.1.15-3) experimental; urgency=medium

  * new maintainer (Closes: #839973)
  * debian/compat: use dh 10
  * debian/control: bump standard to 3.9.8 (no changes)
  * debian/postinst: add "--home /nonexistent" to adduser call
  * debian/source/format: switch format to 3.0 (quilt)

 -- Thorsten Alteholz <debian@alteholz.de>  Fri, 07 Apr 2017 18:39:26 +0200

smstools (3.1.15-2) unstable; urgency=medium

  * QA upload.
  * Set maintainer to Debian QA Group. (see #839973)
  * Add the lsb-base dependency required for the init script.
  * Add Dutch debconf template translation from Frans Spiesschaert.
    (Closes: #766196)

 -- Adrian Bunk <bunk@debian.org>  Sun, 22 Jan 2017 01:39:26 +0200

smstools (3.1.15-1.2) unstable; urgency=high

  * NMU by Jonas Meurer to push the fix into Jessie.
  * Fix initscript (debian/init.d):
    * drop action 'reload' as it does not what policy demands it to do. Use
      'force-reload' in logrotate post-rotate action. This fixes 'force-reload'
      action when used through systemd tools and prevents the smsd daemon
      process from being killed at every log rotation. (closes: #782996)
    * source /lib/lsb/init-functions in order to make systemd tools aware of
      status changes to the daemon that have been caused by invoking the
      initscript directly.

 -- Jonas Meurer <mejo@debian.org>  Mon, 27 Apr 2015 20:45:40 +0200

smstools (3.1.15-1.1) unstable; urgency=low

  * NMU - preventing smstools from entering jessie.
  * Fix syntax error in src/Makefile, override in wrong place.
    debian/patches/fix-makefile-override.patch
    (Closes: #750350)

 -- Russell Stuart <russell-debian@stuart.id.au>  Thu,  9 Oct 2014 17:07:32 +1000

smstools (3.1.15-1) unstable; urgency=low

  * New upstream release:
    - Includes fix for segfault when run with 'smsd -s', so drop the patch
  * Acknowledge NMUs, thanks to Moritz Muehlenhoff and Gregor Herrman
  * Update to standards version 3.9.5; no changes needed
  * Bugfix to init script: Make reload target status-aware, so that stopped
    services are not started on reload
    (Closes: #709014)

 -- Patrick Schoenfeld <patrick.schoenfeld@credativ.de>  Fri, 08 Nov 2013 11:39:45 +0100

smstools (3.1.14-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "smsd -s produces Segmentation fault":
    add new patch processid.patch, backported from 3.1.15 release:
    "When creating a lockfile, main process used incorrect offset -1 with the
    table of names of processes. This caused segmentation fault when smsd was
    compiled using latest compilers."
    (Closes: #682416)

 -- gregor herrmann <gregoa@debian.org>  Mon, 12 Nov 2012 19:03:31 +0100

smstools (3.1.14-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Enable hardened build flags (Closes: #656531)
  * Add Danish debconf translation, thanks to Joe Dalton (Closes: #662043)

 -- Moritz Muehlenhoff <jmm@debian.org>  Sun, 24 Jun 2012 22:43:34 +0200

smstools (3.1.14-1) unstable; urgency=low

  * New upstream release
  * Acknowledge NMU, thanks to Jan Wagner
  * Add a logrotate configuration file
  * Use the new quilt dh series and minimize debian/rules accordingly
  * Add new debconf template translations:
    + Brazilian Portugese translation, thanks to Jef Lui.
      (Closes: #592742)
    + Italian translation, thanks to Stefano Canepa
      (Closes: #608402)
  * Bump Standards-Version; no changes needed

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Thu, 24 Feb 2011 10:28:58 +0100

smstools (3.1.10-0.1) unstable; urgency=low

  * Non-maintainer upload, as requested by maintainer
  * New Upstream version (Closes: #586643)
  * Bump Standards to 3.8.4, no changes needed
  * Define source format 1.0 in debian/source/format
  * Add $remote_fs as dependency for Required-Start and Required-Stop in the
    init script, removed $local_fs, which is implicit

 -- Jan Wagner <waja@cyconet.org>  Mon, 21 Jun 2010 10:19:52 +0200

smstools (3.1.6-1) unstable; urgency=low

  * New upstream version
    (Closes: #494274)
  * Remove patch to fix modem timeouts; upstream includes changes that
    obsolete it
  * Switch to debhelper 7 and minimize the rules file
  * Add a file debian/manpage for manpage installation
  * Add a debian/install file for files which need to be installed
  * Add an ignore.d.server file for logcheck (Closes: #516158)
  * [INTL:ja] Add Japanese po-debconf template translation
    (ja.po); thanks to Hideki Yamane
    (Closes: #558073)
  * Change my email address at some places
  * Make ucf call on purge conditional, so that purge does not fail if
    ucf is not installed.
  * Fix pathname of smsd configuration in ucf call (its smsd.conf not
    smstools.conf)
  * Remove the last changed header from smsd.conf because its annoying
    on every upgrade
  * Fix a bug in the init script, that would cause the daemon to not
    properly run, if /var/run/smstools does not exist, because it
    wouldn't respect its user settings anymore.
  * Make sure a symlink from /var/log/smsd.log to /var/log/smstools/smsd.log
    is created on upgrades, if /var/log/smsd.log exists.
  * Change the logfile path in the default configuration to
    /var/log/smstools/smsd.log

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Fri, 04 Dec 2009 15:05:03 +0100

smstools (3.1.3-4) unstable; urgency=low

  * Merge changes from 3.1-2+lenny1

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Sun, 03 May 2009 14:52:38 +0200

smstools (3.1.3-3) unstable; urgency=low

  * Add missing changelog entry from stable version (3.1-2)
    (Closes: #522376)
  * Don't create /var/run/smstools in postinst nor during the build process,
    because its created in the init script. Thanks lintian.
  * Update Standards-Version
  * Remove patch for disabling of functions that are not thread-safe,
    because it has been applied upstream.

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Fri, 03 Apr 2009 11:34:21 +0200

smstools (3.1.3-2) unstable; urgency=low

  * Upload to unstable
  * Added code to init script that recreates /var/run/smstools with proper
    permissions (taken from dpkg-statoverride) if it went missing.
    (Closes: #510494)
  * Update spanish debconf translation. Thanks to Francisco Javier
    Cuadrado (Closes: #511481)
  * Added swedish debconf translation. Thanks to Martin Bagge (Closes:
    #510555)
  * Added --debconf-ok option to ucf call to fix warning during upgrade
    and for this to work properly also added a versioned ucf dependency.

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Sun, 15 Feb 2009 15:11:45 +0100

smstools (3.1.3-1) experimental; urgency=low

  * New upstream release
  * Upload to experimental because lenny is in freeze
  * Replace errornous $i with the actual path in dpkg-statoverride call for
    /var/log/smsd_stats, to fix the wrong permissions for that directory
  * Call dpkg-statoverride on /var/log/smstools/smsd_stats instead of
    calling it for /var/log/smsd_stats
  * Fix my email address in the Uploaders field.
  * Add a 'set -e' to all maintainer scripts, so that they exit with an error
    if anything goes wrong
  * Make dpkg-statoverride in post-removal maintainer script conditional, so
    that it does not fail, if no override exists

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Fri, 05 Dec 2008 14:08:07 +0100

smstools (3.1-2+lenny1) stable; urgency=low

  * Updated maintainer email address
  * Put patch for disabling logging functions which are not thread-safe into
    series file and fix the patch, so it actually gets applied
    (Closes: #505681)
  * Increase timeouts when sending messages, because otherwise sometimes
    mesages are not sent (Closes: #521802)

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Fri, 03 Apr 2009 11:54:55 +0200

smstools (3.1-2) unstable; urgency=low

  * Disable logging functions which are not thread-safe and not really
    important, to fix sporadical hanging (defunction) of the modem by
    backporting upstream patch from 3.1.2
    (Closes: #505681)

 -- Patrick Schoenfeld <schoenfeld@in-medias-res.com>  Mon, 15 Dec 2008 15:20:52 +0100

smstools (3.1.1-1) experimental; urgency=low

  * New upstream release
    (Closes: #494274)
  * Upload to experimental because lenny is in freeze
  * Add a file README.source to document patch handling
  * Bump Standards-Version
  * Update maintainers email address

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Fri, 08 Aug 2008 09:29:00 +0200

smstools (3.1-1) unstable; urgency=low

  * New upstream version:
      - While running as an unpriviledged user, outgoing files which are not
        writable for smsd are re-created to fix the permissions.
      - Possibility to log phonecalls
      - Regular run feature has been added which can be used to define an
        external script which is executed regulary within a given interval.
        (Closes: #476124)
    (Closes: #480906)
  * Updated watch file to make it work again (previously it only matched 3.0.x
    versions to avoid matching betas, now it matches versions consisting of
    digits and dots instead which seems to be better)
  * Removed patch which were used to workaround permission problems, because it
    is included in upstream sources
  * Added Vcs-Headers
  * Added a patch to re-enable statistics which has been disabled by default
    in the upstream Makefiles
  * Fix startup problems on fresh installations that happens due to the
    /var/log/smsd_stats directory to have wrong permissions by setting
    permissions via dpkg-statoverride in the post-installation script

 -- Patrick Schoenfeld <schoenfeld@in-medias-res.com>  Mon, 12 May 2008 17:57:44 +0200

smstools (3.0.10-2) unstable; urgency=low

  * Use the new Homepage field
  * Updated Standards-Version
  * Fixed erroneous output when smsd is restarted
  * Removed NEWS file and instead added a README.Debian with the most
    important notes
  * Added finish translation for debconf messages.
    Thanks to Esko Arajärvi. (Closes: #457449)
  * Remove usr/share/doc/smstools/scripts from dirs, because the example
    scripts are installed into usr/share/doc/smstools/examples/scripts
  * Add usr/share/doc/smstools/examples/scripts to dirs and remove the call to
    install -d in debian/rules
  * Some cleanups in debian/rules
  * Removed everything related to configure from debian/rules, because it is
    not a autoconf package
  * Replaced (C) with ©, because only the latter is legally entitled
  * Make depend on debconf versioned to make backporters work for oldstable
    backports more easy
  * Change Copyright holder to Copyright in debian/copyright
  * Add ${misc:Depends} to the depends for the case that the debhelpers
    ever need it
  * Some cleanups in debian/postinst
  * Switch to ucf for configuration file handling:
  		+ Add ucf to depends
  		+ Move configuration templates to their own files in /usr/share/smstools
  		  instead of embedding them into the postinst
  		+ Register configuration file with ucf in post-installation script
  		+ Manage configuration changes with ucf
  * Implemented a status target in the init script
  * Don't backup smsd.conf; ucf should do that
  * Don't use @EVENT_HANDLER@ as a default because this way configuration file
    can be invalid under certain circumstances
  * To determine event handler preset in debian/config use only active entries
    (those that are not commented!), and only the first active found.
  * Added debconf-updatepo to the clean target
  * Added missing build dependency po-debconf to package
  * Removed prerm file, because its not needed anymore
  * Removed debian/debian-version because it is not needed
  * Use dpkg-statoverride instead of chown to change permissions of
    directories
  * Change permissions on /var/spool/sms so that members of the smsd group can
    write to it, ensure that the files get the right permissions by setting
    the sticky bit and ensure that members of the group cannot delete each
    others files by setting the restricted deletion bit.
    (Closes: #471128)
  * Comment out 'set -ex' in debian/postinst (it is only needed for
    debug purposes)
  * Added quilt as a patch system
  * Add a patch to workaround permission problems that occur if people don't
    use proper umasks to write to the spool directories
  * Delete symlinks in /var/log on purge
  * Remove Stephan Frings from the copyright file because he isn't a copyright
    holder for versions >= 3.0
  * Use dh_installexamples to install example scripts and configuration files,
    because there seems to be no need for special handling
  * Remove Mark from Uploaders field as he is already maintainer of the
    package and that duplication is not really needed

 -- Patrick Schoenfeld <schoenfeld@in-medias-res.com>  Mon, 07 Apr 2008 09:10:32 +0200

smstools (3.0.10-1) unstable; urgency=low

  [ Mark Purcell ]
  * New upstream release
  * 3.0.9: changes to the PDU checking
    - smstools do not support PDU messages (Closes: #396427)
  * 3.0.9beta2: Running as an unpriviledged user
    - smsd produce files with false group (Closes: #431078)
  * 3.0.9beta2: mode setting is now automatic for PDU's of incoming messages
    - smstools can not decode received messages but I can manualy...
    (Closes: #429374)
  * 3.0.9beta2: Validity period setting now accepts keywords
    - Invalid validity period in config file: year (Closes: #431079)
  * 3.0.9beta2: International Mobile Subscriber Identity (IMSI) asked
    from modem on starup
    - smstools should include a "To: <number>" header in received messages
    (Closes: #429373)
  * Cleanup debian/NEWS
    - Spelling error in NEWS.Debian.gz (Closes: #429372)
  * Install smsd.de.8 - Thanks Michelle
    - german manpage smstools.de.8 (Closes: #432329)
  * Update debian/watch to look for 3.0.x]
  * smsd segfaults on amd64 (Closes: #292639)
  * Upstream changelog moved: dh_installchangelogs doc/history3.html
  * cleanup debian-rules-ignores-make-clean-error

  [ Christian Perrier ]
  * Debconf templates and debian/control reviewed by the debian-l10n-
    english team as part of the Smith review project.
    Closes: #430839, #418572
  * Debconf translation updates:
    - German. Closes: #418575, #432188
    - Galician. Closes: #431439
    - Vietnamese. Closes: #431558
    - Portuguese. Closes: #431619
    - French. Closes: #431727
    - Russian. Closes: #432216
    - Czech. Closes: #433012
    - Basque. Closes: #433762

 -- Mark Purcell <msp@debian.org>  Sat, 21 Jul 2007 11:37:31 +0100

smstools (3.0.2-4) unstable; urgency=medium

  * Urgency medium as this clears a bug introduced in 3.0.2-3 to
    fix the upgrade path:
    Fixed wrong if-condition in pre-removal script
  * Added Spanish debconf templates translation
    Thanks to José Parrella. (Closes: #412458)
  * Added Portuguese translation for debconf messages
    Thanks to Miguel Figueiredo (Closes: #414061)

 -- Patrick Schoenfeld <schoenfeld@in-medias-res.com>  Fri,  9 Mar 2007 11:17:38 +0100

smstools (3.0.2-3) unstable; urgency=medium

  * Urgency medium as this clears a bug in the upgrade path:
    Added prerm script to handle upgrade from 3.0-1 to this version
    (Closes: #403615)
  * Incorporated NMU changes into package.
  * Added Czech translation for smstools (Closes: #408781)
  * Added debian/watch file
  * Made some changes to debian init.d script to fix a few bugs.
    (Closes: #403616)
  * Reconstructed sms3 script from upstream as parts of it got lost

 -- Patrick Schoenfeld <schoenfeld@in-medias-res.com>  Sun,  4 Feb 2007 14:49:44 +0100

smstools (3.0.2-2) unstable; urgency=low

  * Non-maintainer upload to fix a longstanding l10n issue and mark a
    new string as translatable
  * Mark "Other" as translatable in templates. Closes: #402830
  * Debconf templates translations:
    - French added. Closes: #403138

 -- Christian Perrier <bubulle@debian.org>  Thu, 18 Jan 2007 22:09:50 +0100

smstools (3.0.2-1) unstable; urgency=medium

  * New upstream release
    Urgency medium as this clears some RC bugs
    This one now contains support to change the effective userid
    and can therefore run without root privileges.
    (Closes: #169114)
  * Cleaned up build scripts
  * Added debconf configuration to the package
    (Closes: #401844)
  * Added enhanced init script, based on upstream sms3 script, which
    will solve some stop problems.
    (Closes: #401996)
  * Added a static manpage for smsd, removed build rules and build
    dependencies for help2man

 -- Patrick Schoenfeld <schoenfeld@in-medias-res.com>  Fri,  8 Dec 2006 12:52:20 +0100

smstools (3.0-1) unstable; urgency=low

  [ Patrick Schoenfeld ]
  * New upstream release

  [ Mark Purcell ]
  * newer upstream release (Closes: #399089)
  * Update debian/copyright for smstools3
  * Add Patrick Schoenfeld <schoenfeld@in-medias-res.com> to Uploaders:
  * Remove/confiles. Fixes: duplicate-conffile

 -- Mark Purcell <msp@debian.org>  Fri, 17 Nov 2006 22:46:51 +0000

smstools (1.16-1.1) unstable; urgency=high

  * Non-maintainer upload
  * CVE-2006-0083: Apply patch to fix format string issue
    in logging code.  Closes: #347221.

 -- Florian Weimer <fw@deneb.enyo.de>  Mon, 23 Jan 2006 13:49:46 +0100

smstools (1.16-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat,  1 Oct 2005 21:53:14 +0100

smstools (1.15.7-1) unstable; urgency=low

  * New upstream release
  * lintian cleanup copyright

 -- Mark Purcell <msp@debian.org>  Thu,  1 Sep 2005 20:01:37 +0100

smstools (1.14.10-1) unstable; urgency=low

  * New upstream release
  * Spring clean minor issues from BTS
    + Closes: #255031: /var/spool/sms/checked not created
    + Closes: #243225: Slightly new and improved initscript
    + Closes: #169110: Package lacks initscript in /etc/init.d/
    + Closes: #255026: path to getsms/putsms wrong in log files
    + Closes: #255022: init script doesn't detach from terminal

 -- Mark Purcell <msp@debian.org>  Mon, 16 May 2005 10:34:45 +0100

smstools (1.14.8-1) unstable; urgency=low

  * New upstream release (Closes: Bug#292574)
  * Update debian/{watch,copyright,control} to reflect new upstream site

 -- Mark Purcell <msp@debian.org>  Sun,  6 Feb 2005 21:04:15 +0000

smstools (1.14.5-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Mon, 23 Aug 2004 02:09:07 +1000

smstools (1.14.3-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu, 22 Apr 2004 23:57:11 +1000

smstools (1.14.2-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu,  8 Apr 2004 17:31:42 +1000

smstools (1.14.1-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 13 Mar 2004 17:42:05 +1100

smstools (1.14-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu, 26 Feb 2004 18:07:53 +1100

smstools (1.13.1-1) unstable; urgency=low

  * New upstream release
  * Use examples/smsd.conf.easy as default config

 -- Mark Purcell <msp@debian.org>  Tue, 24 Feb 2004 12:12:28 +1100

smstools (1.13-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Tue, 13 Jan 2004 10:00:54 +1100

smstools (1.12.5-2) unstable; urgency=low

  * Build Depends: libmm-dev (>= 1.2.1-1) | libmm11-dev

 -- Mark Purcell <msp@debian.org>  Sat,  3 Jan 2004 14:24:54 +1100

smstools (1.12.5-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Thu,  6 Nov 2003 23:42:10 +1100

smstools (1.12.3-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Wed,  1 Oct 2003 21:50:04 +1000

smstools (1.12-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun,  6 Jul 2003 22:20:22 +1000

smstools (1.9.0-1) unstable; urgency=low

  * New upstream release
  * dh_installexamples examples/.[a-z]* (Closes: Bug#169112)
  * Create /var/spool/sms/sent (Closes: Bug#169113)

 -- Mark Purcell <msp@debian.org>  Sun, 23 Mar 2003 14:29:38 +1100

smstools (1.8.0-2) unstable; urgency=low

  * Rebuild with libmm-dev_1.3.0

 -- Mark Purcell <msp@debian.org>  Sun, 23 Mar 2003 10:18:45 +1100

smstools (1.8.0-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Mon, 30 Dec 2002 08:45:16 +1100

smstools (1.7.4-2) unstable; urgency=low

  * dh_installexamples examples/.[a-z]* (Closes: Bug#169112)
  * Create /var/spool/sms/sent (Closes: Bug#169113)

 -- Mark Purcell <msp@debian.org>  Fri, 15 Nov 2002 20:23:09 +1100

smstools (1.7.4-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun,  8 Sep 2002 22:03:50 +1000

smstools (1.7.1-2) unstable; urgency=low

  * Rebuild against libmm-dev >= 1.2.1-1 (Closes: Bug#155041)

 -- Mark Purcell <msp@debian.org>  Thu,  1 Aug 2002 17:30:36 +1000

smstools (1.7.1-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 27 Jul 2002 20:14:47 +1000

smstools (1.7.0-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 29 Jun 2002 03:36:32 +1000

smstools (1.6.0-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat,  8 Jun 2002 08:03:50 +1000

smstools (1.5.4-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 27 Apr 2002 14:06:36 +1000

smstools (1.5.0-2) unstable; urgency=low

  * lintian cleanup. Add man pages: getsms.1 putsms.1 smsd.8

 -- Mark Purcell <msp@debian.org>  Sun,  7 Apr 2002 22:29:11 +1000

smstools (1.5.0-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sun,  7 Apr 2002 09:26:03 +1000

smstools (1.4.9-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Mon, 18 Mar 2002 22:53:30 +1100

smstools (1.4.7-1) unstable; urgency=high

  * New upstream release. Security fix (see changelog)

 -- Mark Purcell <msp@debian.org>  Sat,  9 Mar 2002 19:54:50 +1100

smstools (1.4.6-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Mon, 18 Feb 2002 18:37:04 +1100

smstools (1.4.2-2) unstable; urgency=low

  * Back out mkstemp patch

 -- Mark Purcell <msp@debian.org>  Sun, 13 Jan 2002 08:07:07 +1100

smstools (1.4.2-1) unstable; urgency=low

  * New upstream release
  * Update debian/watch - still doesn't work, but getting closer :-)

 -- Mark Purcell <msp@debian.org>  Thu, 27 Dec 2001 22:09:46 +1100

smstools (1.3.8-2) unstable; urgency=low

  * Fix broken arch-independent debian/rules

 -- Mark Purcell <msp@debian.org>  Mon, 26 Nov 2001 23:02:47 +1100

smstools (1.3.8-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 24 Nov 2001 22:31:27 +1100

smstools (1.3.5-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Sat, 13 Oct 2001 22:38:35 +1000

smstools (1.2.11-1) unstable; urgency=low

  * New upstream release

 -- Mark Purcell <msp@debian.org>  Wed, 15 Aug 2001 22:58:58 +1000

smstools (1.2.9-1) unstable; urgency=low

  * Initial Release. (closes: bug#106543)

 -- Mark Purcell <msp@debian.org>  Wed, 25 Jul 2001 22:45:09 +1000

Local variables:
mode: debian-changelog
End:
