# translation of ru.po to Russian
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Yuri Kozlov <kozlov.y@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: 3.0.2-4\n"
"Report-Msgid-Bugs-To: smstools@packages.debian.org\n"
"POT-Creation-Date: 2008-02-26 16:34+0100\n"
"PO-Revision-Date: 2007-07-08 18:16+0400\n"
"Last-Translator: Yuri Kozlov <kozlov.y@gmail.com>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Manage smsd configuration automatically?"
msgstr "Управлять настройкой smsd автоматически?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Reject this option if you want to configure smsd manually."
msgstr "Ответьте отрицательно, если хотите настраивать smsd вручную."

#. Type: string
#. Description
#: ../templates:3001
msgid "Global event-handler:"
msgstr "Глобальный обработчик событий (event-handler):"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Please specify an external program or script that will execute, whenever a "
"message is sent or received, or on failures. This is useful for instance "
"when running an email2sms gateway."
msgstr ""
"Укажите внешнюю программу или скрипт, который будет выполняться при отправке "
"или приёме сообщения, или при ошибках. Это полезно, например, при работающем "
"шлюзе email2sms."

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Examples of event-handlers are detailed in /usr/share/doc/smstools/examples."
msgstr "Примеры обработчиков событий даны в /usr/share/doc/smstools/examples."

#. Type: string
#. Description
#: ../templates:4001
msgid "Modem name:"
msgstr "Название модема:"

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"Please specify the short name for the modem device. This is a mandatory "
"setting."
msgstr "Укажите короткое имя устройства модема. Это обязательный параметр."

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../templates:5001 ../templates:7001
msgid "Other"
msgstr "Другое"

#. Type: select
#. Description
#. Type: string
#. Description
#: ../templates:5002 ../templates:6001
msgid "Modem device:"
msgstr "Модемное устройство:"

#. Type: select
#. Description
#: ../templates:5002
msgid ""
"Please specify the modem device. Usually the modem device is /dev/ttyS0 (the "
"first serial port), but your setup may differ; e.g. for a USB device, choose "
"'Other' and specify the full path of the appropriate device node."
msgstr ""
"Укажите модемное устройство. Обычно это /dev/ttyS0 (первый последовательный "
"порт), но в вашей системе может быть и другое; например для устройств, "
"подключающихся по USB, выберите 'Другое' и укажите полный путь к "
"соответствующему устройству."

#. Type: string
#. Description
#: ../templates:6001
msgid "Please specify an optional extra device for the modem."
msgstr "Укажите необязательное дополнительное устройство для модема."

#. Type: select
#. Description
#. Type: string
#. Description
#: ../templates:7002 ../templates:8001
msgid "Modem device speed (bps):"
msgstr "Скорость модема (bps):"

#. Type: select
#. Description
#: ../templates:7002
msgid ""
"Most modems work well with a speed of 19200bps, but some modems may require "
"9600 bps. If your modem does not support any of the baud rates in the list, "
"select 'Other'."
msgstr ""
"Большинство модемов хорошо работают на скорости 19200bps, но для некоторых "
"модемов может требоваться 9600 bps. Если ваш модем не поддерживает ни одной "
"скорости из списка, выберите 'Другое'."

#. Type: boolean
#. Description
#: ../templates:9001
msgid "Receive SMS with this device?"
msgstr "Принимать SMS через это устройство?"

#. Type: boolean
#. Description
#: ../templates:9001
msgid ""
"Please choose whether the device should be used to receive incoming SMS."
msgstr "Укажите, нужно ли использовать устройство для приёма входящих SMS."

#. Type: string
#. Description
#: ../templates:10001
msgid "Modem initialization string:"
msgstr "Строка инициализации модема:"

#. Type: string
#. Description
#: ../templates:10001
msgid ""
"Please specify the modem initialization command. That may be left empty for "
"most modems. Please consult your modem's manual for more details about its "
"supported commands."
msgstr ""
"Введите команду инициализации модема. Это поле можно оставить пустым для "
"большинства модемов. Список поддерживаемых команд можно найти в руководстве "
"по использованию модема."

#. Type: password
#. Description
#: ../templates:11001
msgid "SIM device PIN code:"
msgstr "PIN-код для SIM-карты:"

#. Type: password
#. Description
#: ../templates:11001
msgid "If the device's SIM needs a PIN to be unlocked, please enter it here."
msgstr ""
"Если для SIM-карты требуется вводить PIN для разблокировки, то укажите его "
"здесь."

#. Type: boolean
#. Description
#: ../templates:12001
msgid "Configure another modem?"
msgstr "Настроить другой модем?"

#~ msgid "Previously created configuration file detected"
#~ msgstr "Обнаружен ранее созданный конфигурационный файл"

#~ msgid ""
#~ "The existing configuration file has been moved to /etc/smsd.conf.bak."
#~ msgstr ""
#~ "Существующий конфигурационный файл будет переименован в /etc/smsd.conf."
#~ "bak."
